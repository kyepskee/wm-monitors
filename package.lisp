;;;; package.lisp

(defpackage #:wm-monitors
  (:use #:cl)
  (:local-nicknames (:re :cl-ppcre)
                    (:i :iterate)))
