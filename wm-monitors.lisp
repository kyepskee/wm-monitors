;;;; wm-monitors.lisp

(in-package #:wm-monitors)

(defparameter config
  '(:names ("I" "II" "III" "IV" "V" "VI" "VII" "VIII" "IX" "X")
    :monitors (("DP-0" (1 3 5))
               ("DP-2" (2 4 6 7 8 9 10)))))
(defun monitor-name (monitor)
  (car monitor))
(defun monitor-desktops (monitor)
  (cadr monitor))

(defun run (program-path args)
  (let ((stream (make-string-output-stream)))
    (sb-ext:run-program program-path
                        args
                        :output stream
                        :wait t)
    (get-output-stream-string stream)))

(defun run-xrandr (args)
  (run "/usr/bin/xrandr" args))
(defun run-bspc (args)
  (run "/usr/bin/bspc" args))

(defun get-monitors ()
  (let ((out (run-xrandr '("--query"))))
    (re:all-matches-as-strings "\\w+-\\d(?= connected)" out)))

(defun get-name (n)
  (elt (getf config :names) (- n 1)))

(defun get-mapping ()
  (defun flatten (lst)
    (cond ((null lst) nil)
          ((listp lst)
           (append (flatten (car lst))
                   (flatten (cdr lst))))
          (t (list lst))))
  (flatten (mapcar #'monitor-desktops
                   (getf config :monitors))))

(defun get-actual-position (index)
  (1+ (position index (get-mapping))))

(defun wm-init ()
  (i:iter (i:for monitor in (getf config :monitors))
    (let* ((mname (monitor-name monitor))
           (desktops (monitor-desktops monitor))
           (desktop-string
             (i:iter (i:for desktop in desktops)
               (i:for i upfrom 0)
               (i:with init = "")
               (setf init
                     (concatenate 'string
                                  init
                                  (write-to-string desktop)
                                  " "))
               (i:finally (return (string-trim " " init))))))
      ;; (print desktop-string)
      ;; (print mname)
      (run-bspc (append `("monitor" ,mname "-d")
                        (mapcar #'get-name
                                desktops)))))
  ;; reorder desktops properly
  (run-bspc (append '("wm" "-O")
                    (mapcar #'monitor-name
                            (getf config :monitors)))))


(defun main ()
  (let ((argv sb-ext:*posix-argv*))
    (when (< (length argv) 2)
      (error "Too few arguments, add either a number or -init"))
    (let ((arg (read-from-string (elt argv 1))))
      (trivia:match arg
        ((satisfies integerp)
         (princ (get-actual-position arg)))
        (_
         (progn
           (wm-init)))))))
        ;; (otherwise (error "Invalid argument"))))))
