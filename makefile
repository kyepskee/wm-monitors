all: build install

build:
	sbcl --load wm-monitors.asd \
		--eval '(ql:quickload :wm-monitors)' \
		--eval '(asdf:make :wm-monitors)' \
		--eval '(quit)'

install:
	cp deskchange ~/.local/bin/deskchange
