;;;; wm-monitors.asd

;; :build-operation "program-op"
;; :build-pathname "/home/hubert/.local/bin/")

(asdf:defsystem #:wm-monitors
  :description "Describe wm-monitors here"
  :author "Your Name <your.name@example.com>"
  :license  "Specify license here"
  :version "0.0.1"
  :serial t
  :depends-on (#:cl-ppcre #:iterate #:trivia)
  :components ((:file "package")
               (:file "wm-monitors" :depends-on ("package")))
  :build-operation program-op
  :build-pathname "deskchange"
  :entry-point "wm-monitors::main")
